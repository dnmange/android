package com.example.dellpc.personinformation;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class MajorInAdvancedFragment extends ListFragment implements AdapterView.OnItemClickListener {

    private ArrayList<String> content;
    private String result;
    public MajorInAdvancedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        content = getArguments() != null ? getArguments().getStringArrayList("content"):null;
        result = getArguments() !=null ? getArguments().getString("degree"):null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_major_in_advanced, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        try{
            ArrayAdapter<String> adapter =
                    new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, content);
            setListAdapter(adapter);
            getListView().setOnItemClickListener(this);
        }catch (Exception e){
            Log.d("MajorFragment",e.getMessage());
        }

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Log.i("rew", "position of click " + position );
        Log.i("rew", "id of click " + content.get((int) id));
        MajorSelectedFragment majorSelectedFragment = (MajorSelectedFragment)getActivity();
        result = getAbbreviatedResult(result);
        majorSelectedFragment.selectedMajor(result+content.get((int) id));
    }

    public String getAbbreviatedResult(String result){
        try{
            if(result.indexOf('(')!=-1){
                result = result.substring(result.indexOf('(')+1,result.indexOf(')'));
            }
        }catch(Exception e){
            Log.e("substring",e.getMessage());
        }
        return result+" ";
    }

    public interface MajorSelectedFragment {
        void selectedMajor(String majorValue);
    }
}
