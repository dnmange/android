package com.example.dellpc.personinformation;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MajorSelectionActivity extends AppCompatActivity implements AdvancedDegreeFragment.SelectAdvancedDegree,MajorInAdvancedFragment.MajorSelectedFragment {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_major_selection);
        if (findViewById(R.id.degreeMajorFragment) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            AdvancedDegreeFragment advancedDegreeFragment= new AdvancedDegreeFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.degreeMajorFragment, advancedDegreeFragment);
            transaction.commit();
        }
    }

    public List<String> onSelectDegree() throws IOException{
        return readFile("advancedDegree");
    }

    public void transferDataToMajorFragment(String fileName){
        callMajorFragment(fileName);
    }

    public void callMajorFragment(String fileName){
        MajorInAdvancedFragment majorInAdvancedFragment = new MajorInAdvancedFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("content",readFile(fileName));
        args.putString("degree",fileName);
        majorInAdvancedFragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.degreeMajorFragment, majorInAdvancedFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public ArrayList<String> readFile(String fileName){
        ArrayList<String> content= new ArrayList<String>();
        try{
            InputStream inputStream = getAssets().open(fileName);
            BufferedReader in = new BufferedReader( new InputStreamReader(inputStream));
            String line;
            while((line = in.readLine())!=null){
                content.add(line);
            }
        }catch (IOException exception){
            Log.e("rew", "read Error", exception);
        }
        return content;
    }

    @Override
    public void selectedMajor(String majorValue) {
        Intent selectMajorIntent = getIntent();
        selectMajorIntent.putExtra("majorSelected",majorValue);
        setResult(RESULT_OK,selectMajorIntent);
        finish();
    }


}
