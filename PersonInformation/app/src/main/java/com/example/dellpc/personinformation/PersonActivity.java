package com.example.dellpc.personinformation;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PersonActivity extends AppCompatActivity {

    private EditText firstName;
    private EditText lastName;
    private EditText age;
    private EditText email;
    private EditText phoneNumber;
    private TextView major;
    private static final int SELECT_MAJOR_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        firstName = findViewById(R.id.firstNameField);
        lastName = findViewById(R.id.lastNameField);
        age = findViewById(R.id.ageField);
        email = findViewById(R.id.emailField);
        phoneNumber = findViewById(R.id.phoneField);
        major = findViewById(R.id.majorField);
        Button doneButton = findViewById(R.id.doneButton);

        SharedPreferences personProfile = getSharedPreferences("personFormData",0);
        setSharedPreferences(personProfile);
        if(major.getText().toString().equals(getString(R.string.select_major))){
            major.setTextColor(getResources().getColor(R.color.color_link));
        }else{
            major.setTextColor(getResources().getColor(R.color.default_color));
        }
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences personProfile = getSharedPreferences("personFormData",0);
                SharedPreferences.Editor profileEditor = personProfile.edit();
                profileEditor.putString("firstName",firstName.getText().toString());
                profileEditor.putString("lastName",lastName.getText().toString());
                profileEditor.putString("age",age.getText().toString());
                profileEditor.putString("email",email.getText().toString());
                profileEditor.putString("phoneNumber",phoneNumber.getText().toString());
                profileEditor.putString("major",major.getText().toString());
                profileEditor.commit();
                Context context = getApplicationContext();
                Toast.makeText(context,R.string.storage_message,Toast.LENGTH_SHORT).show();
            }
        });

        major.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.d("OnTouch","clicked");
       //         if(event.getAction() == MotionEvent.ACTION_DOWN)
                Intent selectMajorIntent = new Intent(view.getContext(),MajorSelectionActivity.class);
                startActivityForResult(selectMajorIntent,SELECT_MAJOR_REQUEST);
                return false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode != SELECT_MAJOR_REQUEST) {
            return;
        }
        switch (resultCode){
            case RESULT_OK:
                if(data.getExtras()!=null){
                    major.setText(data.getExtras().getString("majorSelected"));
                }
                major.setTextColor(getResources().getColor(R.color.default_color));
                break;
            case RESULT_CANCELED:
                break;
        }
    }

    protected void setSharedPreferences(SharedPreferences personProfile){
        firstName.setText(personProfile.getString("firstName",""));
        lastName.setText(personProfile.getString("lastName",""));
        age.setText(personProfile.getString("age",""));
        email.setText(personProfile.getString("email",""));
        phoneNumber.setText(personProfile.getString("phoneNumber",""));
        major.setText(personProfile.getString("major","Select Major"));
    }

}
