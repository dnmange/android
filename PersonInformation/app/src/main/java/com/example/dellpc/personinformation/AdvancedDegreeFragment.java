package com.example.dellpc.personinformation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.io.IOException;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdvancedDegreeFragment extends ListFragment implements AdapterView.OnItemClickListener {

    private List<String> content;
    private SelectAdvancedDegree advancedDegree;

    public AdvancedDegreeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_advanced_degree, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        advancedDegree = (SelectAdvancedDegree)getActivity();

        try{
            content = advancedDegree.onSelectDegree();
            ArrayAdapter<String> adapter =
                    new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, content);
            setListAdapter(adapter);
            getListView().setOnItemClickListener(this);
        }catch (IOException exception){
            Log.e("rew", "read Error", exception);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Log.i("rew", "position of click " + position );
        Log.i("rew", "id of click " + content.get((int)id));
        advancedDegree.transferDataToMajorFragment(content.get((int)id));
    }

    public interface SelectAdvancedDegree{
        List<String> onSelectDegree() throws IOException;
        void transferDataToMajorFragment(String fileName);
    }
}
