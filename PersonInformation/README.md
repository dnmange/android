## Abstract
---

* Created a form with fields as First Name, Last Name, Age, Email, Phone, Select Major and Submit done.
* Used SharedPrefrences to store data on submitting data, so that when users kills the app, data can be still retrieved.
* Design and Implemented Intent, Assets, 2 fragments and 2 activity to select a major from given list of majors.  
