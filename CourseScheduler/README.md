## Abstract
---

* Built an android application for students to register for courses offered in SDSU.
* Created login functionality and Dashboard for user which lets user see their registered courses and schedule.
* Implemented filter functionality for course search for efficient results.
* Implemented paging in the application via recycler view for course lists having over 4000 courses. Gained
	230%+ increase in response time via paging when compared to loading all data at once.
