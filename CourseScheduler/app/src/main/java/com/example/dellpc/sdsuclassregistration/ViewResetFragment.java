package com.example.dellpc.sdsuclassregistration;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ViewResetFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ViewResetFragment#} factory method to
 * create an instance of this fragment.
 */
public class ViewResetFragment extends Fragment {

    private OnFragmentInteractionListener onFragmentInteractionListener;
    private String identifier;
    private EditText redId;
    private EditText password;
    private Button submit;
    private SharedPreferences studentProfile;

    public ViewResetFragment() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        void viewReset(String identifier);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        identifier = getArguments() != null ? getArguments().getString("identifier"):null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_reset, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        redId = getView().findViewById(R.id.fragment_red_id_text);
        password = getView().findViewById(R.id.fragment_password_text);
        submit = getView().findViewById(R.id.fragment_submit);

        if(identifier.equals(getString(R.string.reset_student))){
            submit.setText(getString(R.string.reset_student));
        }else if(identifier.equals(getString(R.string.view_schedule))){
            submit.setText(getString(R.string.view_schedule));
        }

        studentProfile = getActivity().getSharedPreferences("studentProfile",0);
        SharedPreferences.Editor studentProfileEditor = studentProfile.edit();
        setSharedPreferences(studentProfile);

        onFragmentInteractionListener = (MainActivity) getActivity();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentProfileEditor.putString("redId",redId.getText().toString());
                studentProfileEditor.putString("password",password.getText().toString());
                studentProfileEditor.commit();

                onFragmentInteractionListener.viewReset(identifier);
            }
        });
    }


    protected void setSharedPreferences(SharedPreferences personProfile){
        password.setText(personProfile.getString("password",""));
        redId.setText(personProfile.getString("redId",""));
    }

}
