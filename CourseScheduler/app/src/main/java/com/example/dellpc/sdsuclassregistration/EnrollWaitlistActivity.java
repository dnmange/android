package com.example.dellpc.sdsuclassregistration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class EnrollWaitlistActivity extends AppCompatActivity implements FilterFragment.FilteFragmentInterface,EnrollWaitlistFragment.EnrollWaitlistFragmentListener{

    private TextView mTextMessage;
    private Bundle args;

    private List<Integer> classes;
    private List<Integer> waitList;
    private int selectedCourseId;
    private TextView noSectionFound;
    private int action=-1;
    private static final int COURSE_DETAILS = 2;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_enrolled:
                    loadEnrolled();
                    return true;
                case R.id.navigation_wait_listed:
                    loadWaitlist();
                    return true;
                case R.id.navigation_add_course:
                    loadAddCourse();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll_waitlist);

        noSectionFound = findViewById(R.id.no_section_found_text);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_enroll_waitlist);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Intent enrollWaitListedIntent= getIntent();
        classes = enrollWaitListedIntent.getIntegerArrayListExtra("classes");
        waitList = enrollWaitListedIntent.getIntegerArrayListExtra("waitlist");

        loadEnrolled();
    }
    protected void loadEnrolled(){
        if(classes.size()==0)
            noSectionFound.setVisibility(View.VISIBLE);
        else
            noSectionFound.setVisibility(View.INVISIBLE);

        args = new Bundle();
        args.putString("identifier",getString(R.string.enrolled));
        args.putIntegerArrayList("classes",(ArrayList<Integer>) classes);
        loadFragment(new EnrollWaitlistFragment(),args);
    }

    protected void loadAddCourse(){
        noSectionFound.setVisibility(View.INVISIBLE);
        args = new Bundle();
        args.putString("identifier",getString(R.string.add_course));
        loadFragment(new FilterFragment(),args);
    }

    protected void loadWaitlist(){
        if(waitList.size()==0)
            noSectionFound.setVisibility(View.VISIBLE);
        else
            noSectionFound.setVisibility(View.INVISIBLE);
        args = new Bundle();
        args.putString("identifier",getString(R.string.wait_listed));
        args.putIntegerArrayList("waitlist",(ArrayList<Integer>) waitList);
        loadFragment(new EnrollWaitlistFragment(),args);
    }

    private void loadFragment(Fragment fragment, Bundle args ){
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_enroll_waitlist_container, fragment);
        transaction.commit();
    }

    private void loadFragmentWithBackStack(Fragment fragment, Bundle args ){
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_enroll_waitlist_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void retrieveClasses(ArrayList<Integer> classes) {
        args = new Bundle();
        args.putString("identifier",getString(R.string.add_course));
        args.putIntegerArrayList("addCourseList",classes);
        loadFragment(new EnrollWaitlistFragment(),args);
    }

    @Override
    public void itemClicked(String identifier, CourseDetails courseDetails) {
        Intent showCourseDetails = new Intent(this,CourseDetailsActivity.class);
        selectedCourseId = courseDetails.getId();
        String details = new Gson().toJson(courseDetails);
        showCourseDetails.putExtra("course", details);
        showCourseDetails.putExtra("identifier",identifier);
        showCourseDetails.putExtra("enroll",classes.size());
        showCourseDetails.putExtra("waitlist",waitList.size());
        startActivityForResult(showCourseDetails,COURSE_DETAILS);
        
        Log.d("EnrollWaitlistActivity",identifier+"");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode != COURSE_DETAILS) {
            return;
        }
        switch (resultCode){
            case RESULT_OK:
                if(data.getExtras()!=null){
                    this.action = (int)data.getExtras().get("action");
                    //changeListBasedOnAction((int)data.getExtras().get("action"));
                }
                break;
            case RESULT_CANCELED:
                break;
        }
    }

    protected void changeListBasedOnAction(){

        switch (action){
            case ApplicationConst.ADD_ENROLL_ACTION:
                classes.add(selectedCourseId);
                loadAddCourse();
                break;
            case ApplicationConst.ADD_WAIT_LIST_ACTION:
                waitList.add(selectedCourseId);
                loadAddCourse();
                break;
            case ApplicationConst.DROP_ENROLL_ACTION:
                classes.remove(getIndexToRemove(classes));
                loadEnrolled();
                break;
            case ApplicationConst.DROP_WAIT_LIST_ACTION:
                waitList.remove(getIndexToRemove(waitList));
                loadWaitlist();
                break;
        }
        this.action = -1;
    }

    protected int getIndexToRemove(List<Integer> list){
        for(int i=0;i<list.size();i++){
            if(selectedCourseId==list.get(i)){
                return i;
            }
        }
        return -1;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        changeListBasedOnAction();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null){
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return true;
    }
}
