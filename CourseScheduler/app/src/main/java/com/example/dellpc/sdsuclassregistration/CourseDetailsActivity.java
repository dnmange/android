package com.example.dellpc.sdsuclassregistration;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;


public class CourseDetailsActivity extends AppCompatActivity {

    private TextView course;
    private TextView courseTitle;
    private TextView section;
    private TextView schedule;
    private TextView units;
    private TextView seats;
    private TextView meeting;
    private TextView fullTitle;
    private TextView prerequisite;
    private TextView description;
    private Button submitButton;

    private String identifier;
    private CourseDetails courseDetails;
    private RequestQueue queue;

    private int enrollSize;
    private int waitlistSize;

    private Intent courseDetailsIntent;

    private static final String tag = "CourseDetailsActivity ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_details);
        queue = RequestQueueSingleton.getInstance(this);

        courseDetailsIntent = getIntent();
        //CourseDetails courseDetails = courseDetailsIntent.getIntegerArrayListExtra("classes");
        identifier = courseDetailsIntent.getStringExtra("identifier");
        String details = courseDetailsIntent.getStringExtra("course");
        courseDetails = new Gson().fromJson(details,CourseDetails.class);
        enrollSize = courseDetailsIntent.getIntExtra("enroll",-1);
        waitlistSize = courseDetailsIntent.getIntExtra("waitlist",-1);

        course = findViewById(R.id.course);
        courseTitle = findViewById(R.id.course_title);
        section = findViewById(R.id.section);
        schedule = findViewById(R.id.schedule);
        units = findViewById(R.id.units);
        seats = findViewById(R.id.seats);
        meeting = findViewById(R.id.meeting);
        fullTitle = findViewById(R.id.full_title);
        description = findViewById(R.id.description);
        prerequisite = findViewById(R.id.prerequisite);
        submitButton = findViewById(R.id.submit_button);

        if(isAdd()){
            submitButton.setText(getString(R.string.add));
        }else if(isDropEnroll() || isDropWaitlist()){
            submitButton.setText(getString(R.string.drop));
        }
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(isAdd()){
                        if(enrollSize<3){
                            if(courseDetails.getSeats()>0){
                                callService(ApplicationConst.ADD_ENROLL_URL,ApplicationConst.ADD_ENROLL_ACTION);
                            }
                            else{
                                showAlertMessageForWaitlist();
                            }
                        }else{
                            displayToast(getString(R.string.max_level_reached));
                        }
                    }else if(isDropWaitlist()){
                        callService(ApplicationConst.DROP_WAIT_LIST_URL,ApplicationConst.DROP_WAIT_LIST_ACTION);
                    }else if (isDropEnroll()){
                        callService(ApplicationConst.DROP_ENROLL_URL,ApplicationConst.DROP_ENROLL_ACTION);
                    }
                }catch (JSONException e){
                    Log.d("CourseDetailsActivity",e.getMessage());
                }
            }
        });

        setFieldsInUI();
    }

    protected void showAlertMessageForWaitlist() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.alert));
        builder.setMessage(getString(R.string.course_is_full_add_waitlist));

        builder.setPositiveButton(getString(R.string.yes_message), (DialogInterface dialog, int which) -> {
            try{
                callService(ApplicationConst.ADD_WAIT_LIST_URL,ApplicationConst.ADD_WAIT_LIST_ACTION);
            }catch (JSONException e){
                Log.d(tag,e.getMessage());
            }
        });

        builder.setNegativeButton(getString(R.string.no_message), (DialogInterface dialog, int which) -> {
            dialog.dismiss();
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    protected void setFieldsInUI(){
        course.setText(courseDetails.getSubject()+""+courseDetails.getCourseNumber());
        courseTitle.setText(courseDetails.getCourseTitle());
        section.setText(courseDetails.getSection());
        schedule.setText(courseDetails.getScheduleNumber());
        CharSequence unitsStr = (CharSequence)(courseDetails.getUnits()+"");
        units.setText(unitsStr);
        CharSequence seatsStr = (CharSequence)(courseDetails.getSeats()+"");
        seats.setText(seatsStr);
        meeting.setText(courseDetails.getMeetingType()+" "+courseDetails.getStarttime()+"-"+courseDetails.getEndTime()+" "+courseDetails.getBuilding()+" "+courseDetails.getInstructor());
        description.setText(courseDetails.getDescription());
        prerequisite.setText(courseDetails.getPrerequisite());
    }

    protected boolean isDropEnroll(){
        return identifier.equals(getString(R.string.enrolled));
    }

    protected boolean isDropWaitlist(){
        return identifier.equals(getString(R.string.wait_listed));
    }

    protected boolean isAdd(){
        return identifier.equals(getString(R.string.add_course));
    }

    protected void callService(String URL, int action) throws JSONException{

        JsonObjectRequest req = new JsonObjectRequest(URL, getParams(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            VolleyLog.v("Response:%n %s", response.toString(4));
                            if(response.has(ApplicationConst.OK)){
                                displayToast(response.getString(ApplicationConst.OK));
                                finishThisActivity(action);
                            }else if(response.has(ApplicationConst.ERROR)){
                                displayToast(response.getString(ApplicationConst.ERROR));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                    }
                }
        );
        queue.add(req);

    }


    protected JSONObject getParams() throws JSONException{
        JSONObject params = new JSONObject();
        SharedPreferences  studentProfile = getSharedPreferences("studentProfile",0);
        params.put("password",studentProfile.getString("password",""));
        params.put("redid",studentProfile.getString("redId",""));
        params.put("courseid",courseDetails.getId());
        return  params;
    }

    protected void finishThisActivity(int action){
        courseDetailsIntent.putExtra("action",action);
        setResult(RESULT_OK,courseDetailsIntent);
        finish();
    }

    protected void displayToast(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
