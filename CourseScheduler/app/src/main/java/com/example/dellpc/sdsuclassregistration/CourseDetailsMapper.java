package com.example.dellpc.sdsuclassregistration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Dell Pc on 01-04-2018.
 */

public class CourseDetailsMapper {
    CourseDetails details;
    HashMap<String,String> map;
    JSONObject obj;
    public CourseDetails getCourseDetailsFromJSON(JSONObject obj) throws JSONException{
        details = new CourseDetails();
        this.obj = obj;
        details.setDescription(obj.has("description") ? obj.getString("description") : "");
        details.setDepartment(obj.has("department") ? obj.getString("department") : "");
        details.setBuilding(obj.has("building") ? obj.getString("building") : "");
        details.setStarttime(obj.has("building")  ? obj.getString("startTime") : "");
        details.setMeetingType(obj.has("meetingType") ? obj.getString("meetingType"): "");
        details.setSection(obj.has("section") ? obj.getString("section") : "");
        details.setEndTime(obj.has("endTime") ? obj.getString("endTime") : "");
        details.setEnrolled(obj.has("enrolled")  ? obj.getInt("enrolled") : -1);
        details.setDays(obj.has("days") ? obj.getString("days"):"");
        details.setPrerequisite(obj.has("prerequisite") ? obj.getString("prerequisite") : "");
        details.setCourseTitle(obj.has("title") ? obj.getString("title") : "");
        details.setId(obj.has("id") ? obj.getInt("id"): -1);
        details.setInstructor(obj.has("instructor") ? obj.getString("instructor"):"");
        details.setScheduleNumber(obj.has("schedule#") ?  obj.getString("schedule#"):"");
        details.setUnits(obj.has("units") ?  obj.getInt("units"):-1);
        details.setRoom(obj.has("room") ?  obj.getString("room") : "");
        details.setWaitlist(obj.has("waitlist") ?  obj.getInt("waitlist") : -1);
        details.setSeats(obj.has("seats") ? obj.getInt("seats"):-1);
        details.setCourseTitle(obj.has("fullTitle") ?  obj.getString("fullTitle"):"");
        details.setSubject(obj.has("subject") ? obj.getString("subject"): "");
        details.setCourseNumber(obj.has("course#") ? obj.getString("course#") : "");

        //getCourseDetailsInMap();
        return details;
    }

    public HashMap<String,String> getCourseDetailsInMap() throws JSONException{
        map = new HashMap<>();
        Iterator iter = obj.keys();
        while(iter.hasNext()){
            String key = (String)iter.next();
            map.put(key,obj.get(key)+"");
        }
        /*map.put("Description",obj.getString("description"));
        map.put("Department",obj.getString("department"));
        map.put("Building",obj.getString("building"));
        map.put("StartTime",obj.getString("startTime"));
        map.put("MeetingType",obj.getString("meetingType"));
        map.put("Section",obj.getString("section"));
        map.put("EndTime",obj.getString("endTime"));
        map.put("Enrolled",obj.getInt("enrolled")+"");
        map.put("Days",obj.getString("days"));
        map.put("Prerequisite",obj.getString("prerequisite"));
        map.put("Title",obj.getString("title"));
        map.put("Id",obj.getInt("id")+"");
        map.put("Instructor",obj.getString("instructor"));
        map.put("Schedule Number",obj.getString("schedule#"));
        map.put("Units",obj.getInt("units")+"");
        map.put("Room",obj.getString("room"));
        map.put("Wait List",obj.getInt("waitlist")+"");
        map.put("Seats", obj.getInt("seats")+"");
        map.put("FullTitle",obj.getString("fullTitle"));
        map.put("Subject",obj.getString("subject"));
        map.put("Course Number",obj.getString("course#"));
        */
        return map;
    }
}
