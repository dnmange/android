package com.example.dellpc.sdsuclassregistration;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Dell Pc on 28-03-2018.
 */

public class RequestQueueSingleton {
    private static RequestQueue instance;
    private static final Object obj = new Object();

    public static RequestQueue getInstance(Context context){
        synchronized (obj) {
            if(instance==null){
                instance = Volley.newRequestQueue(context.getApplicationContext());
            }
            return instance;
        }
    }

}
