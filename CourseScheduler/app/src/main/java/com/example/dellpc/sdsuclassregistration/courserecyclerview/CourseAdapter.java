package com.example.dellpc.sdsuclassregistration.courserecyclerview;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dellpc.sdsuclassregistration.CourseDetails;
import com.example.dellpc.sdsuclassregistration.EnrollWaitlistActivity;
import com.example.dellpc.sdsuclassregistration.EnrollWaitlistFragment;
import com.example.dellpc.sdsuclassregistration.R;

import java.util.List;
/**
 * Created by Dell Pc on 01-04-2018.
 */

public class CourseAdapter extends RecyclerView.Adapter<CourseHolder>{
    private List<CourseDetails> courses;
    private Activity activity;
    private String identifier;
    private RecyclerView recyclerView;

    public CourseAdapter(List<CourseDetails> courses,Activity activity, String identifier, RecyclerView courseRecyclerView){
        this.courses = courses;
        this.activity = activity;
        this.identifier = identifier;
        this.recyclerView = courseRecyclerView;
    }

    @Override
    public CourseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View view = layoutInflater.inflate(R.layout.list_course_overview,parent,false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemPosition = recyclerView.getChildLayoutPosition(view);
                 EnrollWaitlistFragment.EnrollWaitlistFragmentListener enrollWaitlistFragmentListener = (EnrollWaitlistActivity)activity;
                 enrollWaitlistFragmentListener.itemClicked(identifier,courses.get(itemPosition));
            }
        });
        return new CourseHolder(view, parent);
    }

    @Override
    public void onBindViewHolder(CourseHolder holder, int position) {
        holder.bind(courses.get(position));
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

}
