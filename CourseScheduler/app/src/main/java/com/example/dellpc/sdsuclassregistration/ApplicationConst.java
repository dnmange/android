package com.example.dellpc.sdsuclassregistration;

/**
 * Created by Dell Pc on 02-04-2018.
 */

public class ApplicationConst {


    public static final int ADD_ENROLL_ACTION = 0;
    public static final int DROP_ENROLL_ACTION = 1;
    public static final int ADD_WAIT_LIST_ACTION = 2;
    public static final int DROP_WAIT_LIST_ACTION = 3;

    public static final String ADD_ENROLL_URL= "https://bismarck.sdsu.edu/registration/registerclass";
    public static final String DROP_WAIT_LIST_URL = "https://bismarck.sdsu.edu/registration/unwaitlistclass";
    public static final String ADD_WAIT_LIST_URL = "https://bismarck.sdsu.edu/registration/waitlistclass";
    public static final String DROP_ENROLL_URL="https://bismarck.sdsu.edu/registration/unregisterclass";

    public static final String ADD_STUDENT_URL= "https://bismarck.sdsu.edu/registration/addstudent";
    public static final String CLASS_DETAILS_URL = "https://bismarck.sdsu.edu/registration/classdetails";
    public static final String CLASS_ID_LIST_URL = "https://bismarck.sdsu.edu/registration/classidslist";
    public static final String SUBJECT_LIST_URL  = "https://bismarck.sdsu.edu/registration/subjectlist";
    public static final String RESET_STUDENT = "https://bismarck.sdsu.edu/registration/resetstudent";
    public static final String STUDENT_CLASSES = "https://bismarck.sdsu.edu/registration/studentclasses";

    public static final String OK  = "ok";
    public static final String ERROR  = "error";

    public static final String NAME_PATTERN = "^[\\p{L} .'-]+$";
}
