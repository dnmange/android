package com.example.dellpc.sdsuclassregistration;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements AddStudentFragment.OnFragmentInteractionListener, ViewResetFragment.OnFragmentInteractionListener{

    private Bundle args;
    private static final int ENROLL_WAITLIST = 1;
    private RequestQueue queue;
    private String identifier;
    private SharedPreferences studentProfile;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_register:
                    loadRegister();
                    return true;
                case R.id.navigation_schedule:
                    args = new Bundle();
                    args.putString("identifier",getString(R.string.view_schedule));
                    loadFragment(new ViewResetFragment(),args);
                    return true;
            }
            return false;
        }
    };

    protected void loadRegister(){
        loadFragment(new AddStudentFragment(),new Bundle());
    }

    private void loadFragment(Fragment fragment, Bundle args ){
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        /*if(firstTimeInitialization){

        }*/
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState!=null)
            return;

        loadRegister();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public void addStudent() {
        //callStudentApi();
        this.identifier = getString(R.string.register_student);
        startEnrollWaitListActivity();
    }

    @Override
    public void viewReset(String identifier) {
        this.identifier = identifier;
        startEnrollWaitListActivity();
    }

    public void startEnrollWaitListActivity(){
        studentProfile =  getSharedPreferences("studentProfile",0);
        queue = RequestQueueSingleton.getInstance(this);
        final String URL;
        if (identifier.equals(getString(R.string.reset_student))){
            URL = ApplicationConst.RESET_STUDENT;
        }else{
            URL = ApplicationConst.STUDENT_CLASSES;
        }
        JsonObjectRequest req = new JsonObjectRequest(URL, new JSONObject(getParams()),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            VolleyLog.v("Response:%n %s", response.toString(4));
                            if(identifier.equals(getString(R.string.reset_student))){
                                parseResetResponse(response);
                            }else{
                                parseViewScheduleResponse(response);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                    }
                }
        );

        queue.add(req);

    }

    protected void parseViewScheduleResponse(JSONObject response) throws JSONException{
        Iterator<String> iter = response.keys();
        if(response.has(ApplicationConst.ERROR)){
            displayToast((String) response.get(ApplicationConst.ERROR));
            return;
        }
        List<Integer> classes = new ArrayList<Integer>();
        List<Integer> waitlist = new ArrayList<Integer>();
        while(iter.hasNext()){
            String key = iter.next();
            JSONArray responseArray = response.getJSONArray(key);
            if(key.equals("classes")){
                for(int i=0;i<responseArray.length();i++){
                    classes.add((Integer) responseArray.get(i));
                }
            }else if(key.equals("waitlist")){
                for(int i=0;i<responseArray.length();i++){
                    waitlist.add((Integer) responseArray.get(i));
                }
            }
        }
        Intent enrollWaitlist = new Intent(this,EnrollWaitlistActivity.class);
        //enrollWaitlist.p
        enrollWaitlist.putIntegerArrayListExtra("classes", (ArrayList<Integer>) classes);
        enrollWaitlist.putIntegerArrayListExtra("waitlist", (ArrayList<Integer>) waitlist);
        startActivityForResult(enrollWaitlist,ENROLL_WAITLIST);
    }

    protected void parseResetResponse(JSONObject response) throws JSONException{
        Iterator<String> iter = response.keys();
        while(iter.hasNext()){
            String key = iter.next();
            if(key.equals("ok")){
                displayToast(response.getString("ok"));
            }
        }
    }

    protected Map<String, String> getParams(){
        Map<String,String> params = new HashMap<>();
        params.put("password",studentProfile.getString("password",""));
        params.put("redid",studentProfile.getString("redId",""));
        return  params;
    }

    protected void displayToast(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode != ENROLL_WAITLIST) {
            return;
        }
        switch (resultCode){
            case RESULT_OK:
                /*if(data.getExtras()!=null){
                    major.setText(data.getExtras().getString("majorSelected"));
                }
                major.setTextColor(getResources().getColor(R.color.default_color));
                */break;
            case RESULT_CANCELED:
                break;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null){
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return true;
    }
}
