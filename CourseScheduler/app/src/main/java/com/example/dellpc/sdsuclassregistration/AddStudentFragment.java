package com.example.dellpc.sdsuclassregistration;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddStudentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class AddStudentFragment extends Fragment {

    private RequestQueue queue;

    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText redId;
    private EditText password;
    private Button addStudentButton;
    private SharedPreferences studentProfile;
    private Pattern pattern;

    private OnFragmentInteractionListener onFragmentInteractionListener;

    public interface OnFragmentInteractionListener {
        void addStudent();
    }

    public AddStudentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_student, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        firstName = getView().findViewById(R.id.firstNameText);
        lastName = getView().findViewById(R.id.lastNameText);
        email = getView().findViewById(R.id.emailAddressText);
        redId = getView().findViewById(R.id.redIdText);
        password = getView().findViewById(R.id.passwordText);
        addStudentButton  = getView().findViewById(R.id.addStudentButton);
        pattern = Pattern.compile(ApplicationConst.NAME_PATTERN);
        studentProfile = getActivity().getSharedPreferences("studentProfile",0);

        SharedPreferences.Editor studentProfileEditor = studentProfile.edit();
        setSharedPreferences(studentProfile);

        onFragmentInteractionListener = (MainActivity)getActivity();

        addStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!areFieldsValid()){
                    return;
                }
                studentProfileEditor.putString("firstName",firstName.getText().toString());
                studentProfileEditor.putString("lastName",lastName.getText().toString());
                studentProfileEditor.putString("email",email.getText().toString());
                studentProfileEditor.putString("password",password.getText().toString());
                studentProfileEditor.putString("redId",redId.getText().toString());
                studentProfileEditor.commit();
                handleClick();
            }
        });
    }

    protected boolean areFieldsValid(){
        String firstNameVal = firstName.getText().toString();
        String lastNameVal = lastName.getText().toString();

        if(!firstNameVal.isEmpty() && !firstNameVal.equals("")){
            if(!isNameValid(firstNameVal)){
                displayToast(getString(R.string.invalid_first_name));
                return false;
            }
        }

        if(!lastNameVal.isEmpty() && !lastNameVal.equals("")){
            if(!isNameValid(lastNameVal)){
                displayToast(getString(R.string.invalid_last_name));
                return false;
            }
        }

        return true;
    }

    protected boolean isNameValid(String val){
        Matcher matcher = pattern.matcher(val);
        return matcher.matches();
    }
    protected void handleClick(){

        queue = RequestQueueSingleton.getInstance(getContext());
        final String URL = ApplicationConst.ADD_STUDENT_URL;
        JsonObjectRequest req = new JsonObjectRequest(URL, new JSONObject(getParams()),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            VolleyLog.v("Response:%n %s", response.toString(4));
                            Iterator<String> iter = response.keys();
                            while(iter.hasNext()){
                                String key = iter.next();
                                if(key.equals("ok")){
                                    displayToast(response.getString("ok"));
                                    onFragmentInteractionListener.addStudent();
                                }else if(key.equals("error")){
                                    displayToast(response.getString("error"));
                                }
                                break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                    }
                }
        );
        queue.add(req);
    }


    protected Map<String, String> getParams(){
        Map<String,String> params = new HashMap<>();
        params.put("firstname",studentProfile.getString("firstName",""));
        params.put("lastname",studentProfile.getString("lastName",""));
        params.put("email",studentProfile.getString("email",""));
        params.put("password",studentProfile.getString("password",""));
        params.put("redid",studentProfile.getString("redId",""));
        return  params;
    }
    protected void setSharedPreferences(SharedPreferences studentProfile){
        firstName.setText(studentProfile.getString("firstName",""));
        lastName.setText(studentProfile.getString("lastName",""));
        email.setText(studentProfile.getString("email",""));
        password.setText(studentProfile.getString("password",""));
        redId.setText(studentProfile.getString("redId",""));
    }

    protected void displayToast(String message){
        Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
    }
}
