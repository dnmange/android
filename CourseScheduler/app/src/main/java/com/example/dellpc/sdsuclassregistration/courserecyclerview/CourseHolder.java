package com.example.dellpc.sdsuclassregistration.courserecyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dellpc.sdsuclassregistration.CourseDetails;
import com.example.dellpc.sdsuclassregistration.R;

/**
 * Created by Dell Pc on 01-04-2018.
 */

public class CourseHolder extends RecyclerView.ViewHolder{
    private TextView courseNumber;
    private TextView courseTitle;

    public CourseHolder(View view, ViewGroup parent){
        super(view);
        //View view = inflater.inflate(R.layout.list_course_overview,parent,false);
        courseNumber =  itemView.findViewById(R.id.course_number);
        courseTitle =  itemView.findViewById(R.id.course_title);
    }


    public void bind(CourseDetails courseDetails){
        courseNumber.setText("Course No:"+courseDetails.getSubject()+""+courseDetails.getCourseNumber());
        courseTitle.setText("Course Title:"+courseDetails.getCourseTitle());
    }

}
