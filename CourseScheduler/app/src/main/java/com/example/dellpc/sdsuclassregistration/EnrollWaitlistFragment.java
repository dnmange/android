package com.example.dellpc.sdsuclassregistration;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.dellpc.sdsuclassregistration.courserecyclerview.CourseAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class EnrollWaitlistFragment extends Fragment {

    private RecyclerView courseRecyclerView;
    private CourseAdapter courseAdapter;
    private RequestQueue queue;
    private String identifier;
    private ArrayList<Integer> classIds;
    private List<CourseDetails> courses;
    private  int count = 0;

    public interface EnrollWaitlistFragmentListener{
        void itemClicked(String identifier, CourseDetails courseDetails);
    }

    public EnrollWaitlistFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enroll_waitlist, container, false);
        identifier = getArguments() != null ? getArguments().getString("identifier"):null;
        if(identifier.equals(getString(R.string.enrolled))){
            classIds = getArguments() != null ? getArguments().getIntegerArrayList("classes"):null;
        }else if(identifier.equals(getString(R.string.wait_listed))){
            classIds = getArguments() != null ? getArguments().getIntegerArrayList("waitlist"):null;
        }else if(identifier.equals(getString(R.string.add_course))){
            classIds = getArguments() != null ? getArguments().getIntegerArrayList("addCourseList"):null;
        }
        courseRecyclerView = (RecyclerView) view
                .findViewById(R.id.course_recycler_view);
        courseRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();
        return view;
    }

    private void updateUI(){
        queue = RequestQueueSingleton.getInstance(getContext());
        courses = new ArrayList<CourseDetails>();
        for(int i=0;i<classIds.size();i++){
            callCourseDetailsService(classIds.get(i),i);
        }
    }

    protected void callCourseDetailsService(int classId,int index){
        final String URL = ApplicationConst.CLASS_DETAILS_URL+"?classid="+classId;

        JsonObjectRequest req = new JsonObjectRequest(URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            VolleyLog.v("Response:%n %s", response.toString(4));
                            CourseDetailsMapper mapper = new CourseDetailsMapper();
                            courses.add(mapper.getCourseDetailsFromJSON(response));
                            count++;
                            if(count==classIds.size()){
                                count=0;
                                courseAdapter = new CourseAdapter(courses,getActivity(),identifier,courseRecyclerView);
                                courseRecyclerView.setAdapter(courseAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

        queue.add(req);
    }
    protected void displayToast(String message){
        Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
    }

}
