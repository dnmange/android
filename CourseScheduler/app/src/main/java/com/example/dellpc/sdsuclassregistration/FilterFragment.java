package com.example.dellpc.sdsuclassregistration;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 */
public class FilterFragment extends Fragment{

    private Spinner majorListSpinner;
    private Spinner levelSpinner;
    private String levelSelected;
    private int majorSelected;
    private RequestQueue queue;
    private ArrayList<String> majorList;
    private ArrayList<Integer> classIds;
    private ArrayAdapter<String> adapterMajorList;
    private ArrayAdapter<CharSequence> adapterLevel;
    private HashMap<String,Integer> majorListMap;

    private EditText startTime;
    private EditText endTime;
    private Button searchButton;
    private Button resetButton;

    private Pattern pattern;
    private static final String TIME_PATTERN = "([01]?[0-9]|2[0-3])[0-5][0-9]";

    public FilterFragment() {
        // Required empty public constructor
    }

    public interface FilteFragmentInterface{
        void retrieveClasses(ArrayList<Integer> classes);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_filter, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        queue = RequestQueueSingleton.getInstance(getContext());

        pattern = Pattern.compile(TIME_PATTERN);
        majorListSpinner = (Spinner)getView().findViewById(R.id.major_list_spinner);
        levelSpinner = (Spinner)getView().findViewById(R.id.level_spinner);
        startTime = (EditText) getView().findViewById(R.id.start_time_text);
        endTime = (EditText) getView().findViewById(R.id.end_time_text);
        searchButton = (Button) getView().findViewById(R.id.search_button);
        resetButton = (Button) getView().findViewById(R.id.reset_button);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSearch();
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleReset();
            }
        });

        adapterLevel = ArrayAdapter.createFromResource(getContext(),
                R.array.level, android.R.layout.simple_spinner_item);
        adapterLevel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        retrieveMajorList();
        levelSpinner.setAdapter(adapterLevel);
        levelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                handleLevelItem(adapterView,view,i,l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    protected void handleSearch(){
        if(isSearchValid()){
            String queryParams = "subjectid="+majorSelected;
            String startTimeValue = startTime.getEditableText().toString();
            String endTimeValue = endTime.getEditableText().toString();
            queryParams += ((levelSelected!=null && !levelSelected.equals(""))  ? "&level="+levelSelected.toLowerCase() : "");
            queryParams += ((startTimeValue  != null && !startTimeValue.equals("")) ? "&starttime="+startTimeValue : "");
            queryParams += ((startTimeValue!=null && !endTimeValue.equals("")) ? "&endtime="+endTimeValue : "");

            final String URL = ApplicationConst.CLASS_ID_LIST_URL+"?"+queryParams;

            JsonArrayRequest req = new JsonArrayRequest(URL,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try{
                                VolleyLog.v("Response:%n %s", response.toString(4));
                                classIds = new ArrayList<>();
                                for(int i=0;i<response.length();i++) {
                                    classIds.add((Integer) response.get(i));
                                }
                                if(classIds.size()!=0){
                                    FilteFragmentInterface filteFragmentInterface = (EnrollWaitlistActivity) getActivity();
                                    filteFragmentInterface.retrieveClasses(classIds);
                                }else{
                                    displayToast(getString(R.string.no_course_found));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.e("Error: ", error.getMessage());
                }
            });

            queue.add(req);
        }
    }

    protected boolean isSearchValid(){
        String startVal = startTime.getText()+"";
        String endVal = endTime.getText()+"";
        if(!startVal.equals("")&& !startVal.isEmpty()){
            if(!isValidTime(startVal)){
                displayToast(getString(R.string.invalid_start_time));
                return false;
            }
        }
        if(!endVal.equals("")&& !endVal.isEmpty()){
            if(!isValidTime(endVal)){
                displayToast(getString(R.string.invalid_end_time));
                return false;
            }
        }
        return true;
    }

    protected boolean isValidTime(String val)
    {
        Matcher matcher = pattern.matcher(val);
        return matcher.matches();
    }

    protected void handleReset(){
        levelSpinner.setAdapter(adapterLevel);
        majorListSpinner.setAdapter(adapterMajorList);
        startTime.setText("");
        endTime.setText("");
    }

    protected void retrieveMajorList(){
        final String URL = ApplicationConst.SUBJECT_LIST_URL;
        JsonArrayRequest req = new JsonArrayRequest(URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            majorListMap = new HashMap<>();
                            majorList = new ArrayList<>();
                            VolleyLog.v("Response:%n %s", response.toString(4));
                            for(int i=0;i<response.length();i++){
                                JSONObject major = (JSONObject) response.get(i);
                                majorListMap.put(major.getString("title"),major.getInt("id"));
                                majorList.add(major.getString("title"));
                            }
                            adapterMajorList = new ArrayAdapter<String>(getActivity(),  android.R.layout.simple_spinner_item, majorList);
                            adapterMajorList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            majorListSpinner.setAdapter(adapterMajorList);
                            majorListSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    handleMajorListItem(adapterView, view, i, l);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                            //majorList
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

        queue.add(req);
    }

    protected void handleMajorListItem(AdapterView<?> adapterView, View view, int i, long l){
        majorSelected = majorListMap.get(adapterView.getItemAtPosition(i));
    }

    protected void handleLevelItem(AdapterView<?> adapterView, View view, int i, long l){
        levelSelected = (String)adapterView.getItemAtPosition(i);
    }

    protected void displayToast(String message){
        Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
    }
}
