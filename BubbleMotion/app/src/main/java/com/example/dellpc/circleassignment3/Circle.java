package com.example.dellpc.circleassignment3;

public class Circle {

    private float radius=27;
    private float threshold;
    private float xCenter;
    private float yCenter;
    private static final int increment= 10;
    private float xVelocity = 0.0f;
    private float yVelocity = 0.0f;

    public Circle(float xCenter, float yCenter, float threshold){
        this.xCenter = xCenter;
        this.yCenter = yCenter;
        this.threshold = threshold;
    }
    public void setXcenter(float xCenter){
        this.xCenter = xCenter;
    }

    public void setyCenter(float yCenter){
        this.yCenter = yCenter;
    }
    public float getRadius(){
        return radius;
    }

    public float getxCenter(){
        return this.xCenter;
    }

    public float getyCenter(){
        return this.yCenter;
    }

    public float getThreshold(){
        return threshold;
    }

    public void incrementRadius(){
        if (this.radius+increment<=threshold)
            this.radius += increment;
    }

    public float getxVelocity() {
        return xVelocity;
    }

    public void setxVelocity(float xVelocity) {
        this.xVelocity = xVelocity;
    }

    public float getyVelocity() {
        return yVelocity;
    }

    public void setyVelocity(float yVelocity) {
        this.yVelocity = yVelocity;
    }

}
