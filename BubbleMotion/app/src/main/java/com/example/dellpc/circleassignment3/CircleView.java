package com.example.dellpc.circleassignment3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

/**
 * TODO: document your custom view class.
 */
public class CircleView extends View implements View.OnTouchListener {

    private static Paint blackFill;
    private static List<Circle> listOfCircle;
    private static float canvasWidth;
    private static float canvasHeight;
    private static float xVelocity;
    private static float yVelocity;
    private static int actionCode;
    private static final int delayMilliseconds = 200;

    private int circleToMoveIndex = -1;
    private boolean viewInMovingMode = false;
    private boolean longPress = false;
    private GestureDetectorCompat detectGesture;
    private VelocityTracker velocityTracker;
    private TimerTask task;
    private Handler handler = new Handler();

    static {
        blackFill = new Paint();
        blackFill.setColor(Color.BLACK);
        listOfCircle = new ArrayList<Circle>();
    }

    public class CircleGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";

        @Override
        public boolean onDown(MotionEvent motionEvent) {
            Log.d(DEBUG_TAG,"onDown: " + motionEvent.toString());
            return true;
        }

        @Override
        public void onLongPress(MotionEvent motionEvent) {
            Log.d(DEBUG_TAG,"onLongPresed: " + motionEvent.toString());
            longPress = true;
            handleLongPress();
        }
    }

    public CircleView(Context context) {
        super(context);
        detectGesture = new GestureDetectorCompat(context,new CircleGestureListener());
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnTouchListener(this);
        detectGesture = new GestureDetectorCompat(context,new CircleGestureListener());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvasWidth = canvas.getWidth();
        canvasHeight = canvas.getHeight();
        int i=0;
        if (!listOfCircle.isEmpty()){
            for (Circle objCircle : listOfCircle){
                modifyingCircleVelocity(canvas,objCircle);
                canvas.drawCircle(objCircle.getxCenter(),objCircle.getyCenter(),objCircle.getRadius(),blackFill);
                i++;
            }
        }
        if(viewInMovingMode){
            invalidate();
        }
    }

    /*Before drawing circle everytime velocity is calculated based on position(i.e detecting edges)
    and current velocity*/
    private void modifyingCircleVelocity(Canvas canvas, Circle objCircle) {
        float currentXVelocity = objCircle.getxVelocity();
        float currentYVelocity = objCircle.getyVelocity();
        float radius = objCircle.getRadius();
        if(objCircle.getxCenter() - radius + currentXVelocity <= 0
                || objCircle.getxCenter() + radius + currentXVelocity >= canvas.getWidth()) {
            currentXVelocity = -currentXVelocity;
            objCircle.setxVelocity(currentXVelocity);
        }else if(objCircle.getyCenter() - radius + currentYVelocity <= 0
                || objCircle.getyCenter() + radius + currentYVelocity >= canvas.getHeight()) {
            currentYVelocity = -currentYVelocity;
            objCircle.setyVelocity(currentYVelocity);
        }
        objCircle.setXcenter(objCircle.getxCenter() + currentXVelocity);
        objCircle.setyCenter(objCircle.getyCenter() + currentYVelocity);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        actionCode = action & MotionEvent.ACTION_MASK;

        this.detectGesture.onTouchEvent(motionEvent);
        switch (actionCode){
            case MotionEvent.ACTION_DOWN:
                return handleActionDown(motionEvent);
            case MotionEvent.ACTION_MOVE:
                return handleActionMove(motionEvent);
            case MotionEvent.ACTION_UP:
                return handleActionUp(motionEvent);
        }
        return false;
    }

    private boolean handleActionDown(MotionEvent motionEvent){
        if(listOfCircle.isEmpty()){
            listOfCircle.add(new Circle(motionEvent.getX(),motionEvent.getY(),calculateThreshold(motionEvent)));
            return true;
        }

        velocityTracker = VelocityTracker.obtain();
        velocityTracker.addMovement(motionEvent);

        for (int i=0;i<listOfCircle.size();i++){
            if(isTouchInsideCircle(listOfCircle.get(i),motionEvent.getX(),motionEvent.getY())){
                circleToMoveIndex = i;
                return true;
            }
        }

        listOfCircle.add(new Circle(motionEvent.getX(),motionEvent.getY(),calculateThreshold(motionEvent)));
        return true;
    }

    /*detect pressure size as well to increase radius as finger is still on the screen*/
    private boolean handleActionMove(MotionEvent motionEvent){
        if(circleToMoveIndex==-1) {
            listOfCircle.get(listOfCircle.size() - 1).incrementRadius();
        }else{
            viewInMovingMode = true;
        }
        if(viewInMovingMode){
            velocityTracker.addMovement(motionEvent);
        }
        invalidate();
        return true;
    }

    private void handleLongPress(){
        final Circle objCircle = listOfCircle.get(listOfCircle.size()-1);
        task = new TimerTask() {
            @Override
            public void run() {
                if(circleToMoveIndex==-1){
                    objCircle.incrementRadius();
                }
                invalidate();
                handler.postDelayed(this, delayMilliseconds);
            }
        };
        handler.postDelayed(task, delayMilliseconds);
    }

    /*Remove handler as finger is not on the screen, handler is used to increase the radius of circle*/
    private boolean handleActionUp(MotionEvent motionEvent){
        if(longPress){
            handler.removeCallbacks(task);
        }
        if(viewInMovingMode){
            velocityTracker.addMovement(motionEvent);
            velocityTracker.computeCurrentVelocity(1);
            xVelocity = velocityTracker.getXVelocity();
            yVelocity = velocityTracker.getYVelocity();
            if(circleToMoveIndex!=-1){
                listOfCircle.get(circleToMoveIndex).setxVelocity(xVelocity);
                listOfCircle.get(circleToMoveIndex).setyVelocity(yVelocity);
            }
        }
        if(circleToMoveIndex!=-1){
            circleToMoveIndex = -1;
        }
        invalidate();
        return true;
    }

    /*threshold is used to determine till which circle radius is incremented*/
    private float calculateThreshold(MotionEvent motionEvent){
        float threshold = checkDistanceFromWall(motionEvent);
        if(!listOfCircle.isEmpty()){
            for (Circle objCircle : listOfCircle){
                float distanceFromCurrentCircle = calculateDistance(objCircle.getxCenter(),objCircle.getyCenter(),motionEvent.getX(),motionEvent.getY()) - objCircle.getRadius();
                if(distanceFromCurrentCircle>0 & distanceFromCurrentCircle<threshold)
                    threshold = distanceFromCurrentCircle;
            }
        }
        return threshold;
    }

    /*distance from wall is calculated so that on increasing the radius it doesn't go beyond the wall*/
    private float checkDistanceFromWall(MotionEvent motionEvent){
        float minimumDistanceFromWall = Float.MAX_VALUE;
        float leftWallDistance = calculateDistance(motionEvent.getX(),motionEvent.getY(),0,motionEvent.getY());
        float rightWallDistance = calculateDistance(motionEvent.getX(),motionEvent.getY(),canvasWidth,motionEvent.getY());
        float upperWallDistance = calculateDistance(motionEvent.getX(),motionEvent.getY(),motionEvent.getX(),0);
        float bottomWallDistance = calculateDistance(motionEvent.getX(),motionEvent.getY(),motionEvent.getX(),canvasHeight);

        minimumDistanceFromWall = minimumDistanceFromWall>leftWallDistance ? leftWallDistance : minimumDistanceFromWall;
        minimumDistanceFromWall = minimumDistanceFromWall>rightWallDistance ? rightWallDistance : minimumDistanceFromWall;
        minimumDistanceFromWall = minimumDistanceFromWall>upperWallDistance ? upperWallDistance : minimumDistanceFromWall;
        minimumDistanceFromWall = minimumDistanceFromWall>bottomWallDistance ? bottomWallDistance : minimumDistanceFromWall;

        return minimumDistanceFromWall;
    }

    private float calculateDistance(float startingPointX,float startingPointY, float endingPointX,float endingPointY){
        double distanceX = Math.pow(Math.abs(startingPointX-endingPointX),2);
        double distanceY = Math.pow(Math.abs(startingPointY-endingPointY),2);
        return (float)Math.sqrt(distanceX + distanceY);
    }

    /*Method to check whether to draw new circle or move circle*/
    public boolean isTouchInsideCircle(Circle objCircle,float touchPointX,float touchPointY){
        float distanceFromCenter = calculateDistance(objCircle.getxCenter(),objCircle.getyCenter(),touchPointX,touchPointY);
        return distanceFromCenter<=objCircle.getRadius();
    }
}
