## Abstract
---

* Built a bubble doodle android app which lets user create bubbles with long touches and play with them. 
* Created simplified physics engine for bubble motion in direction indicated by finger touches.
* Implemented velocity tracker and gestures listeners to set speed of the movement of bubbles.
* Incorporated bouncing off functionality from walls in the physics engine by using directions of the circle.

