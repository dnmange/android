## Abstract
---

This app was made as part of the Android class assignment. Converts inr to usd and vice versa.

## Test Cases Covered:
---

1. on rotation, value in the fields is restored
2. If user inputs both inr and usd field, than the value is converted from field where touch is focussed.
3. on landscape mode next action is replaced with done so that touch focus is not changed.
4. on entering only ".", validation message is thrown to user.
5. every Non numeric values on screen is stored in strings.xml