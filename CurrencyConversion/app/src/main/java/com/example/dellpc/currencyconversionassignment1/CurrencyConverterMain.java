package com.example.dellpc.currencyconversionassignment1;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.text.DecimalFormat;

public class CurrencyConverterMain extends AppCompatActivity {

    private EditText inrText;
    private EditText usdText;
    private Button convertButton;
    private AlertDialog alertDialog;

    private static String convertInputTo;
    private static final double inrConversionRate = 63.89;
    private static final double usdConversionRate = 0.016;
    private static DecimalFormat decimalFormat = new DecimalFormat(".##");
    private static String validateMessage;

    /*onCreate is called when activity is started*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inrText = findViewById(R.id.inrValue);
        usdText = findViewById(R.id.usdValue);
        convertButton = findViewById(R.id.convertButton);

        alertDialog = new AlertDialog.Builder(CurrencyConverterMain.this).create();
        alertDialog.setTitle(getString(R.string.alert));

        //attaching onClick listener for convert button
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                convertButtonHandler();
            }
        });

        //set convertInputTo to inr if touch is on inr text
        inrText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                convertInputTo = getString(R.string.inr);
                return false;
            }
        });

        //set convertInputTo to usd if touch is on usd text
        usdText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                convertInputTo = getString(R.string.usd);
                return false;
            }
        });
    }

    //this handler is called when user clicks on convert button
    protected void convertButtonHandler(){
        Editable inrValue = inrText.getText();
        Editable usdValue = usdText.getText();

        String inrFocus = getString(R.string.inr);
        String usdFocus = getString(R.string.usd);

        /*If both fields have been entered than convert the field where touch is focused
          else convert field which has non empty value*/
        if(validateInput(inrValue.toString()) && validateInput(usdValue.toString())){
            if(convertInputTo.equals(inrFocus)){
                convertCurrencyToAndFrom(usdConversionRate,inrValue,usdText);
            }else if(convertInputTo.equals(usdFocus)){
                convertCurrencyToAndFrom(inrConversionRate,usdValue,inrText);
            }
        }else if(validateInput(inrValue.toString())){
            convertCurrencyToAndFrom(usdConversionRate,inrValue,usdText);
        }else if(validateInput(usdValue.toString())){
            convertCurrencyToAndFrom(inrConversionRate,usdValue,inrText);
        }else{
            alertDialog.setMessage(validateMessage);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialog.show();
        }

    }

    /*Validation function to check user inputs*/
    protected boolean validateInput(String validateField){
        if(validateField.equals("") || validateField.equals(".")){
            validateMessage = getString(R.string.invalid_input);
            return false;
        }
        return true;
    }

    /*actual conversion of currency is done by convertCurrencyToAndFrom function*/
    protected void convertCurrencyToAndFrom(Double conversionRate, Editable sourceValue, EditText targetText){
        double source = Double.parseDouble(sourceValue.toString());
        String convertedValue = decimalFormat.format(source*conversionRate);
        targetText.setText(convertedValue);
    }
}
